from svgpath_encodefont import *
from random import randint


with open('content.txt', 'r') as f:
    txt = f.read()

largeur_max = 260
hauteur_max = 380

facteur_x = 30
facteur_y = 18

#chasse = 2*facteur_x

text_to_print = [*txt]
#print(texte_to_print)

path_final = []

decalage_x = 0
decalage_y = 0

#lines = 1

path_final.append("G17 G21 (mm) G90 G94 G54 F800")
path_final.append("M4 S1000 F800")

mes_formes["space"] = [[1.0, 1.0]]

#print(text_to_print)

for s in text_to_print:
    lettre = str(s)
    if lettre == " " or lettre == "\n" or lettre == "-":
        lettre = "space"
    elif lettre not in mes_formes:
        lettre = "space"
        
    if (float(mes_formes[lettre][0][0])*facteur_x)+decalage_x > largeur_max:
        #lines += 1
        decalage_x = 0
        decalage_y += facteur_y + 10
        facteur_x = randint(15, 50)
        facteur_y = randint(20, 50)
        
    if (float(mes_formes[lettre][0][1])*facteur_y)+decalage_y > hauteur_max:
        pass
    else:
        path_final.append("G00 X%s Y%s" % (str((float(mes_formes[lettre][0][0])*facteur_x)+decalage_x), str(-abs((float(mes_formes[lettre][0][1])*facteur_y)+decalage_y))))
        path_final.append("M4 S1000")
        max_x = (float(mes_formes[lettre][0][0])*facteur_x)+decalage_x
        y_value = -abs(float(mes_formes[lettre][0][1])*facteur_y)
        for m in mes_formes[lettre][1:]:
            path_final.append("G01 X%s Y%s" % (str((float(m[0])*facteur_x)+decalage_x), str(-abs((float(m[1])*facteur_y)+decalage_y))))
            if (float(m[0])*facteur_x)+decalage_x > max_x:
                max_x = (float(m[0])*facteur_x)+decalage_x
            y_value = (float(m[1])*facteur_y)
        #path_final.append("G00 X%s Y%s" % (str(float(max_x + chasse)), str(y_value)))
        path_final.append("M3 S1000")
        decalage_x = max_x
        #print(path_final)

with open('geopendown.gcode', 'w') as f:
    f.write('\n'.join(path_final))