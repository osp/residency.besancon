# Read SVG into a list of path objects and list of dictionaries of attributes 

# TO DO: superpose points 

from svgpathtools import *
paths, attributes = svg2paths('x.svg')

mes_formes = {}

def path1_is_contained_in_path2(path1, path2):
	assert path2.isclosed()  # This question isn't well-defined otherwise

	# find a point that's definitely outside path2
	xmin, xmax, ymin, ymax = path2.bbox()

	# We need to offset the rectangle, so points on the rectangle are not intersecting
	offset = 1
	seg1 = Line((xmin-offset)+1j*(ymin-offset), (xmax+offset)+1j*(ymin-offset))
	seg2 = Line((xmax+offset)+1j*(ymin-offset), (xmax+offset)+1j*(ymax+offset))
	seg3 = Line((xmax+offset)+1j*(ymax+offset), (xmin-offset)+1j*(ymax+offset))
	seg4 = Line((xmin-offset)+1j*(ymax+offset), (xmin-offset)+1j*(ymin-offset))
	path3 = Path(seg1, seg2, seg3, seg4)  # A path traversing the cubic and then the line

	if path3.intersect(path1):
		return False
	
	B = (xmin + 2) + 1j*(ymax + 2)

	A = path1.start  # pick an arbitrary point in path1
	AB_line = Path(Line(A, B))
	number_of_intersections = len(AB_line.intersect(path3))
	#print (number_of_intersections)
	if number_of_intersections % 2:  # if number of intersections is odd
		return True
	else:
		return False


# return x and y coordinates in a frame, constrain between 0 and 1
def compute_points(x, y, xmin, ymin, width, height):
	computed_x = (x-xmin)/width
	computed_y = (y-ymin)/height
	xy =[round(computed_x,2), round(computed_y,2)]
	return xy


# Store red_frames
red_frames = []

for index,p in enumerate (paths):
    path_attributes = attributes[index] #params
    #print(path_attributes)

    style = path_attributes['style']
    #print (style)
    color = style.partition('#')[2]
    colorHex = color[:6]
    #print (colorHex)
    if colorHex == "ff0000":
        red_frames.append(p)

#print ("Nbr of Red Frames: ", len(red_frames) )


# Store pairs of 1 path contained in 1 path
letters = []
for index, p in enumerate (paths):	
	for f in red_frames:
		if p != f and path1_is_contained_in_path2(p, f):
			letter_attributes = attributes[index] #params
			character = letter_attributes['inkscape:label']
			#print("Just added: ", character)
			letters.append([p, f, character])

#print ("Nbr of Letters: ", len(letters) )

for p, f, character in letters:
	
	#get character 
	#print ("Processing char: ",character)

	# get origin of frame by storing top left point
	xmin, xmax, ymin, ymax = f.bbox()
	#print ("Bounding Box", xmin, xmax, ymin, ymax)
	width = xmax-xmin
	height = ymax-ymin
	#print ("Width/Height", width, height)
	
	letter_points = []

	# iterate through points
	for start, end in p:
		#print (start.real, start.imag, end.real, end.imag)
		letter_points.append([start.real, start.imag])

	# Manually add last point, as we just added starting point of each segment
	last_segment = p[-1]
	endOfPath = last_segment.end
	letter_points.append([last_segment.end.real, last_segment.end.imag])
	#print ("Nbr Of Points: ", len(letter_points))
	
	
	coord = []
	for x,y in letter_points:
		coord.append(compute_points(x,y,xmin, ymin, width, height))


	mes_formes[character] = coord
	#print("---")

#print (mes_formes.keys()) 
