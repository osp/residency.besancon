#!/bin/bash

for i in {1..20}
do
  vpype read specimen.svg linesimplify -t $i write spork_$i.svg
done
