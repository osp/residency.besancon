654: work.balsamine.2021-2022
https://gitlab.constantvzw.org/osp/work.balsamine.2021-2022
git@gitlab.constantvzw.org:osp/work.balsamine.2021-2022.git


602: work.balsamine.2020-2021
https://gitlab.constantvzw.org/osp/work.balsamine.2020-2021
git@gitlab.constantvzw.org:osp/work.balsamine.2020-2021.git


508: work.balsamine.2019-2020
https://gitlab.constantvzw.org/osp/work.balsamine.2019-2020
git@gitlab.constantvzw.org:osp/work.balsamine.2019-2020.git


448: work.balsamine.2018-2019
https://gitlab.constantvzw.org/osp/work.balsamine.2018-2019
git@gitlab.constantvzw.org:osp/work.balsamine.2018-2019.git


426: archives.balsa
https://gitlab.constantvzw.org/osp/archives.balsa
git@gitlab.constantvzw.org:osp/archives.balsa.git


388: foundry.ume
https://gitlab.constantvzw.org/osp/foundry.ume
git@gitlab.constantvzw.org:osp/foundry.ume.git


353: work.balsamine.2017-2018
https://gitlab.constantvzw.org/osp/work.balsamine.2017-2018
git@gitlab.constantvzw.org:osp/work.balsamine.2017-2018.git


351: work.balsamine.www
https://gitlab.constantvzw.org/osp/work.balsamine.www
git@gitlab.constantvzw.org:osp/work.balsamine.www.git

177: work.balsamine.2016-2017
https://gitlab.constantvzw.org/osp/work.balsamine.2016-2017
git@gitlab.constantvzw.org:osp/work.balsamine.2016-2017.git


176: work.balsamine.2015-2016
https://gitlab.constantvzw.org/osp/work.balsamine.2015-2016
git@gitlab.constantvzw.org:osp/work.balsamine.2015-2016.git


175: work.balsamine.2014-2015
https://gitlab.constantvzw.org/osp/work.balsamine.2014-2015
git@gitlab.constantvzw.org:osp/work.balsamine.2014-2015.git


174: work.balsamine.2013-2014
https://gitlab.constantvzw.org/osp/work.balsamine.2013-2014
git@gitlab.constantvzw.org:osp/work.balsamine.2013-2014.git


173: work.balsamine.2012-2013
https://gitlab.constantvzw.org/osp/work.balsamine.2012-2013
git@gitlab.constantvzw.org:osp/work.balsamine.2012-2013.git


172: work.balsamine.2011-2012
https://gitlab.constantvzw.org/osp/work.balsamine.2011-2012
git@gitlab.constantvzw.org:osp/work.balsamine.2011-2012.git


121: live.balsa-bootstrap
https://gitlab.constantvzw.org/osp/live.balsa-bootstrap
git@gitlab.constantvzw.org:osp/live.balsa-bootstrap.git