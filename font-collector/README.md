# Collection of scripts to collect fonts from a set of repositories

## Install

The scripts use jinja2 to render the templates, install it with:

```
pip install -r requirements.txt
```

## Usage
- `list_projects.py` lists the projects and their id's for projects with balsa in their name.
- `find_fonts.py` calls gitlab api and loops through file tree, generates a json file `balsa_fonts.json`, long run time
- `list_fonts.py` lists fonts in project repositories
- `make_font_list.py` very similar to `list_fonts.py`, but generates html and downloads the fonts. Use like: `python3 make_font_list.py > index.html`

