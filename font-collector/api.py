from urllib.request import URLError, urlopen, Request
from urllib.parse import quote_plus, urlencode, urljoin, quote
from settings import CACHE_DIR, API_URL, DEBUG, MAX_PAGES, PER_PAGE

import os.path
import os
import json
import datetime
import time

import hashlib

"""

  Collection of classes to interact with the gitlab api.

"""

def debug (msg):
  if DEBUG:
    print(msg)

class ApiError(Exception):
    def __init__(self, url, what):
        self.url = url
        self.what = what
    def __str__(self):
        return '(%s) => %s'%(self.url,self.what)

class ApiCall (object):
  def __init__ (self, api_path, query = None, api_url = API_URL, cache_dir = CACHE_DIR):
    self._cache = None
    self.api_path = list(map(str, api_path))
    self.api_url = api_url
    self.cache_dir = cache_dir
    self.query = query

  @property
  def cache_location(self):
    cache_path_raw = '.'.join(self.api_path)

    if self.query:
      chunks = [ '{}={}'.format(k, v) for k, v in self.query.items() ]
      query_string = '&'.join(chunks)
      cache_path_raw += query_string

    cache_path_hashed = hashlib.sha256(cache_path_raw.encode('utf-8')).hexdigest()

    return os.path.join(self.cache_dir, '{}.json'.format(cache_path_hashed))

  @property
  def url(self):
    return urljoin(self.api_url, '/'.join(self.api_path))

  @property
  def has_cache(self):
    return os.path.exists(self.cache_location)

  def invalidate_cache(self):
    if self.has_cache:
      debug('Invalidating cache `{}`'.format(self.cache_location))
      os.unlink(self.cache_location)

  def expire_cache_after (self, expiry_date):
    if self.has_cache:
      timestamp = os.path.getctime(self.cache_location)
      # TODO: implement timezone-info
      cache_date = datetime.datetime.fromtimestamp(timestamp)

      if expiry_date > cache_date:
        self.invalidate_cache()

  def write_cache(self, data):
    try:
      with open(self.cache_location, 'w') as h:
        h.write(self.prepare_cache(data))
    except ApiError as e:
      json.dump({
        'reason': e.what,
        'timestamp': datetime.datetime.now().timestamp()               
      }, open(self.cache_location, 'w'))
      data = []

    self._cache = data

  def read_cache(self):
    debug('Hit cache {} ({})'.format(self.url, self.cache_location))
    if not self._cache:
      with open(self.cache_location, 'r') as h:
        self._cache = self.parse_cache(h.read())
        
    return self._cache

  def _prepare_request(self, url):
    return Request(url)

  def _make_api_call(self, url, query = None):
    if query:
      url = '{}?{}'.format(url, urlencode(query))

    try:
      debug('Loading `{}`'.format(url))
      res = urlopen(self._prepare_request(url))
      return self.parse_api_result(res)
    except URLError as e:
      if hasattr(e, 'reason'):
        raise ApiError(url, e.reason)
      # elif hasattr(e, 'code'):
      #   raise ApiError(url, e.code)
    return None

  def read_from_api (self):
    try:
      _, data = self._make_api_call(self.url, self.query)
      return data
    except ApiError:
      return None

  def get (self, force_call=False):
    if force_call or not self.has_cache:
      data = self.read_from_api()
      self.write_cache(data)
       
    return self.read_cache()

  def parse_api_result (self, response):
    return (self.parse_api_response_headers(response), self.parse_api_response_body(response))

  def parse_api_response_headers (self, response):
    return { k: v for (k,v) in response.getheaders() }

  def parse_api_response_body (self, response):
    return response.read().decode()

  def prepare_cache (self, raw):
    return raw
  
  def parse_cache (self, raw):
    return raw


class MultiPageMixin:
  """
    Returns values for the call. If the request is paginated go through
    all pages
  """  
  def read_from_api(self):
    query = self.query.copy() if self.query else {}


    page = 1
    data = []
    query['per_page'] = PER_PAGE
    max_pages = MAX_PAGES

    try:
      while page and page < max_pages:
        query['page'] = page
        headers, page_data = self._make_api_call(self.url, query)
        data.extend(page_data)
        page = int(headers['x-next-page']) if 'x-next-page'in headers and headers['x-next-page'] else None
        if page:
          time.sleep(1/10)
    except ApiError:
      return data


class ApiCallJsonMixin:
  def parse_api_response_body (self, response):
    debug('JSON parsing response')
    return json.loads(response.read())

  def prepare_cache(self, raw):
    return json.dumps(raw)

  def parse_cache(self, raw):
    return json.loads(raw)


class ApiCallJsonMultiPage (MultiPageMixin, ApiCallJsonMixin, ApiCall):
  pass

class ApiCallJson (ApiCallJsonMixin, ApiCall):
  pass

class ApiHeadCallJSON (ApiCallJsonMixin, ApiCall):
  gitlab_prefix = 'x-gitlab-'

  def _prepare_request(self, url):
    return Request(url, method='HEAD')

  def parse_api_response_body (self, response):
    return {
      k[len(self.gitlab_prefix):]: v for k, v in response.getheaders() if k.lower().startswith(self.gitlab_prefix)
    }

class ApiCallRaw (ApiCall):
  @property
  def cache_location(self):
    return os.path.join(self.cache_dir, '{}.data'.format('.'.join(self.api_path)))

def get_group (group_id):
  return ApiCallJson(['groups', group_id])

def get_project (project_id):
  return ApiCallJson(['projects', project_id])

# A way to make a call, cache and be able te remove the cache when needed
# prepare a call, set of url and local cache file
def get_projects (group_id):
  return ApiCallJsonMultiPage(['groups', group_id, 'projects'])

def get_commits (project_id):
  return ApiCallJsonMultiPage(['projects', project_id, 'repository', 'commits'])

def get_file (project_id, file_path, ref):
  return ApiHeadCallJSON(['projects', project_id, 'repository', 'files', quote(file_path, safe='')], { 'ref': ref })

def get_commit (project_id, commit_sha):
  return ApiCallJson(['projects', project_id, 'repository', 'commits', commit_sha])

def get_tree (project_id, path=None):
  query = { 'path': path } if path else None
  return ApiCallJsonMultiPage(['projects', project_id, 'repository', 'tree'], query=query)

"""
  Get the raw contents of file.

  Returns a `ApiCallRaw`
"""
def get_raw (project_id, file_id):
  return ApiCallRaw(['projects', project_id, 'repository', 'blobs', file_id, 'raw'])