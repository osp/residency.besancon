"""
  This script loops through all Balsa repositories and tries
  to find font files in it.
"""

from settings import projects, INDEXED_EXTENSIONS

project_fonts = {}

import time
import os.path
from api import get_tree
import json

# print(get_tree(projects[0]).get())

def find_fonts_in_project (project_id, path=None):
  fonts = []

  entries = get_tree(project_id, path).get()

  for entry in entries:
    if entry['type'] == 'tree':
      time.sleep(.05)
      print('Opening subpath', entry['path'])
      fonts.extend(find_fonts_in_project(project_id, entry['path']))

    elif entry['type'] == 'blob':
      _, ext = os.path.splitext(entry['name'])

      if ext in INDEXED_EXTENSIONS:
        fonts.append(entry)
  
  return fonts

for project_id in projects:
  print('Reading {}'.format(project_id))
  project_fonts[project_id] = find_fonts_in_project(project_id)

json.dump(project_fonts, open('balsa_fonts.json', 'w'), ensure_ascii=False, indent=2)