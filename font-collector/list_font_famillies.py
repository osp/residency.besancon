import json
# from settings import projects
from api import get_project
import re
import os

fonts = json.load(open('balsa_fonts.json', 'r'))
unique_fonts = {}

file_web_url_pattern = '{web_url}/-/blob/{branch}/{path}'
download_url_pattern = '{web_url}/-/blob/{branch}/{path}'




# * extension
# * -webfont
# * mincho / gothic / autre
# *

for project_id in sorted(fonts.keys()):
  project = get_project(int(project_id)).get()
  
  for font in fonts[project_id]:

    (basename, ext) = os.path.splitext(font['name'])

    # split on '-' or '_', and to lowercase
    font['id'] = '_'.join(re.split('[-,_]', basename.lower()))

    # if not seen a font with this name syntax
    if font['id'] not in unique_fonts:

        # add it to a unique_fonts object
        font['projects'] = {}
        font['projects'][project['id']] = project
        unique_fonts[font['id']] = font

    # if already there, just add project id
    else:
        existing_font = unique_fonts[font['id']]
        if project['id'] not in existing_font['projects']:
            existing_font['projects'][project['id']] = project
            # print(json.dumps(project, indent=4))

font_famillies = {}

for font_id, font in unique_fonts.items():
    # print(font_id)
    # for project_id, project in font['projects'].items():
    #     print('  ' + project['name'])

    id0 = font_id.split('_')
    best_inter = []
    for font_id_bis, font_bis in unique_fonts.items():
        if font_id != font_id_bis:
            id1 = font_id_bis.split('_')
            i = 0
            while i < len(id0) and id0[i] in id1:
                i = i + 1
            inter = id0[:i]
            if len(inter) > len(best_inter):
                best_inter = inter
    
    familly_id = '_'.join(best_inter)
    if familly_id not in font_famillies:
        font_famillies[familly_id] = {}
    font_famillies[familly_id][font_id] = font

for familly_id, familly in font_famillies.items():
    print(familly_id)
    for font_id, font in familly.items():
        print('  ' + font_id)
    print()

# json.dumps(unique_fonts)

#   print()
#   print('# ' + project['name'])
#   print()
#   print(project['web_url'])
#   print(project['ssh_url_to_repo'])
#   print()

#   for font in fonts[project_id]:
#     if font['name'] not in seen_fonts:
#       seen_fonts.append(font['name'])
#       # print(font)
#       print(font['name'])
#       print(font['path'])
#       print(file_web_url_pattern.format(web_url=project['web_url'], branch=project['default_branch'], path=font['path']))
#       print()

#     # else:
#       # print("Saw {} before".format(font['name']))

#     project_fonts.append(font)