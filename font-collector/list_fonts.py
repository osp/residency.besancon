import json
# from settings import projects
from api import get_project

fonts = json.load(open('balsa_fonts.json', 'r'))
seen_fonts = []

file_web_url_pattern = '{web_url}/-/blob/{branch}/{path}'
download_url_pattern = '{web_url}/-/blob/{branch}/{path}'

for project_id in sorted(fonts.keys()):
  project = get_project(int(project_id)).get()
  project_fonts = []

  print()
  print('# ' + project['name'])
  print()
  print(project['web_url'])
  print(project['ssh_url_to_repo'])
  print()

  for font in fonts[project_id]:
    if font['name'] not in seen_fonts:
      seen_fonts.append(font['name'])
      # print(font)
      print(font['name'])
      print(font['path'])
      print(file_web_url_pattern.format(web_url=project['web_url'], branch=project['default_branch'], path=font['path']))
      print()

    # else:
      # print("Saw {} before".format(font['name']))

    project_fonts.append(font)