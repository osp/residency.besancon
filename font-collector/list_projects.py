from api import get_projects
from settings import GROUP_ID

# List all projects
projects = get_projects(GROUP_ID).get()

print("Candidates")
for project in projects:
  if "balsa" in project['name'] or "ume" in project['name']:
    print("{}: {}\n{}\n{}\n\n".format(project['id'], project['name'], project['web_url'], project['ssh_url_to_repo']))


print("\n\nAll projects")
for project in projects:
  print("{}: {}".format(project['id'], project['name']))