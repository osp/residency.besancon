from email.mime import base
from api import get_project, get_file, get_commit
import json
import os
from settings import FONT_DIR, PLACEHOLDER_TEXT, PREVIEWED_EXTENSIONS, FILE_WEB_URL_PATTERN, DOWNLOAD_URL_PATTERN
from jinja2 import Environment, FileSystemLoader, select_autoescape
from utils import create_nested_address_in_tree, normalize_basename, download_file
import time
from datetime import datetime
import copy

env = Environment(
    loader=FileSystemLoader("templates"),
    autoescape=select_autoescape()
)

template = env.get_template("font-list.html")
template_sorted = env.get_template('font-list-sorted.html')
template_commit_list = env.get_template('commit-list.html')
template_recursive = env.get_template("font-list-recursive.html")
template_character_stack = env.get_template("character-stack.html")
template_character_overview = env.get_template("character-overview.html")
template_font_overview = env.get_template("overview.html")

"""
  Merge branches that have only one child into their parent label.
"""
def simplify_sparse_branches (tree):
  tree_simplified = {}

  for key, value in tree.items():
    if key != '__font' and key != '__type':
      value_simplified = simplify_sparse_branches(value)

      if not '__font' in value_simplified and len(value_simplified) == 2:
        key_simplified = list(value_simplified.keys())[1]
        tree_simplified[key + ' ' + key_simplified] = value_simplified[key_simplified]
      else:
        tree_simplified[key] = value_simplified
    
    else:
      tree_simplified[key] = value
      
  return tree_simplified

fonts = json.load(open('balsa_fonts.json', 'r'))

seen_fonts = {}

"""
  {
    str:basename: {
      str:ext: {
        name: string,
        web_url: string,
        download_url: string
      },
      ...
    },
    ...
  }
"""
structured_font_list = {}
webfonts = {}
named_font_list = {}
commits = []
commits_seen = []

for project_id in sorted(fonts.keys()):
  project = get_project(int(project_id)).get()

  for font in fonts[project_id]:
    basename, ext = os.path.splitext(font['name'])
    basename = normalize_basename(basename)
    address_parts = basename.split(' ')
    insertion_point = create_nested_address_in_tree(address_parts, structured_font_list)
    download_url = DOWNLOAD_URL_PATTERN.format(web_url=project['web_url'], branch=project['default_branch'], path=font['path']).replace(' ', '%20').replace('?', '%3F')
    web_url = FILE_WEB_URL_PATTERN.format(web_url=project['web_url'], branch=project['default_branch'], path=font['path']).replace(' ', '%20').replace('?', '%3F')
    
    if basename not in seen_fonts:
      seen_fonts[basename] = {}
      local_basepath, _ = os.path.splitext(os.path.join('webfonts', font['name']))

      webfonts[basename] = {
        'name': font['name'],
        'basename': basename,
        'address_parts': address_parts,
        'local_path': os.path.join('webfonts', font['name']),
        'template_safe_name': '{}'.format(basename, ext[1:]).replace(' ', '_'),
        'local_basepath': local_basepath,
        'web_url': web_url,
        'extensions': [ ext ],
        'occurences': [ ],
        'seasons': [ project ]
      }

    if ext not in webfonts[basename]['extensions']:
      webfonts[basename]['extensions'].append(ext)

    file_info = get_file(project_id, font['path'], project['default_branch']).get()
    commit = get_commit(project_id, file_info['last-commit-id']).get()

    if commit not in commits_seen:
      commits_seen.append(commit)
      commits.append(copy.copy(commit))
      commits[-1]['occurences'] = [(font['name'], web_url)]
      commits[-1]['project'] = project
      commits[-1]['project_name'] = project['name']
      commits[-1]['fonts'] = [ webfonts[basename] ]
    else:
      idx = commits_seen.index(commit)
      commits[idx]['occurences'].append((font['name'], web_url))

      if basename not in commits[-1]['fonts']:
        commits[-1]['fonts'].append(webfonts[basename])


    webfonts[basename]['occurences'].append(
      (web_url, copy.copy(commit))
    )

    if project not in webfonts[basename]['seasons']:
      webfonts[basename]['seasons'].append(project)

    if ext not in seen_fonts[basename]:
      seen_fonts[basename][ext] = {
        'name': font['name'],
        'basename': basename,
        'web_url': web_url,
        'download_url': download_url,
        'template_safe_name': '{}--{}'.format(basename, ext[1:]).replace(' ', '_'),
        'local_path': os.path.join('fonts', font['name']),
        'seasons': [ project ]
      }

      
      local_path_absolute = os.path.join(FONT_DIR, font['name'])

      if not os.path.exists(local_path_absolute):
        download_file(local_path_absolute, download_url)

    if ext not in insertion_point:
      insertion_point[ext] = {
        'type': '__font',
        'name': font['name'],
        'basename': basename,
        'web_url': web_url,
        'download_url': download_url,
        'template_safe_name': '{}--{}'.format(basename, ext[1:]).replace(' ', '_'),
        'local_path': os.path.join('fonts', font['name']),
        'seasons': [ project ]
      }

    elif project not in seen_fonts[basename][ext]['seasons']:
      seen_fonts[basename][ext]['seasons'].append(project)

# print(structured_font_list)

webfonts_structured = {}

for name, font in webfonts.items():
  insertion_point = create_nested_address_in_tree(font['basename'].split(' '), webfonts_structured)
  insertion_point['__font'] = font

webfonts_structured = simplify_sparse_branches(webfonts_structured)

with open('index.html', 'w') as h:
  h.write(template.render(seen_fonts=seen_fonts, PLACEHOLDER_TEXT=PLACEHOLDER_TEXT, PREVIEWED_EXTENSIONS=PREVIEWED_EXTENSIONS))

with open('index-recursive.html', 'w') as h:
  h.write(template_recursive.render(
    fonts=structured_font_list, PLACEHOLDER_TEXT=PLACEHOLDER_TEXT, PREVIEWED_EXTENSIONS=PREVIEWED_EXTENSIONS))

with open('index-sorted.html', 'w') as h:
  fonts = sorted(webfonts.items(), key=lambda kv: kv[0])
  h.write(template_sorted.render(fonts=webfonts_structured))

webfonts_css = ''

for key, value in list(webfonts.items()):
  if not '.otf' in value['extensions'] and not '.ttf' in value['extensions']:
    del webfonts[key]
 
for fontface in webfonts.values():
  if '.otf' in fontface['extensions']:
    webfonts_css += """@font-face {{
  font-family: {templatename};
  src: url("{local_basepath}.woff2") format('woff2'),
        url("{local_basepath}.woff") format('woff'),
        url("{local_basepath}.otf") format('opentype');
}}

""".format(templatename=fontface['template_safe_name'], local_basepath=fontface['local_basepath'])
  
  elif '.ttf' in fontface['extensions']:
    webfonts_css += """@font-face {{
  font-family: {templatename};
  src: url("{local_basepath}.woff2") format('woff2'),
       url("{local_basepath}.woff") format('woff'),
       url("{local_basepath}.ttf") format('truetype');
}}

""".format(templatename=fontface['template_safe_name'], local_basepath=fontface['local_basepath'])

with open('webfonts.css', 'w') as h:
  h.write(webfonts_css)

with open('character-stack.html', 'w') as h:
  h.write(template_character_stack.render(webfonts=webfonts.values()))

with open('character-overview.html', 'w') as h:
  h.write(template_character_overview.render(webfonts=webfonts.values()))

with open('fonts-overview.html', 'w') as h:
  h.write(template_font_overview.render(webfonts=webfonts.values()))

json.dump(webfonts, open('webfonts.json', 'w'))

for commit in commits:
  commit['committed_date'] = datetime.strptime(commit['committed_date'][:-10], "%Y-%m-%dT%H:%M:%S")

  if commit['author_name'] == 'Dorian':
    commit['author_name'] = 'Doriane'

commit_seen_basenames = []

with open('commits.html', 'w') as h:
  last = None
  group = []
  grouped = []

  for commit in sorted(commits, key=lambda c: c['committed_date']):
    commit['new_fonts'] = []
    
    if commit['project'] != last and group:
      grouped.append((last, group))
      group = []

    for font in commit['fonts']:
      if font['basename'] not in commit_seen_basenames:
        commit_seen_basenames.append(font['basename'])
        commit['new_fonts'].append(font)

    last = commit['project']
    group.append(commit)

  if group:
    grouped.append((last, group))
    

  h.write(template_commit_list.render(grouped=grouped))
