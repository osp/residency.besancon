from email.mime import base
from api import get_project
import json
from settings import FONT_DIR, PLACEHOLDER_TEXT, PREVIEWED_EXTENSIONS, FILE_WEB_URL_PATTERN, DOWNLOAD_URL_PATTERN
import os.path
from utils import normalize_basename, create_nested_address_in_tree
from graphutils import make_name_generator
from graphviz import Graph


def make_node (graph, label, name_generator, connect_to=None, fontsize=15):
  nodename = name_generator() 
  graph.node(nodename, label=label, shape='plaintext', fontsize=str(fontsize), fontname='Ume P Gothic Balsa')
  
  if connect_to:
    graph.edge(nodename, connect_to, style='dotted')

  return nodename


def graph_subtree (subtree, graph, name_generator, parent_nodename=None):
  for key, entry in subtree.items():
    if key != '__type':
      if entry['__type'] != 'font':
        nodename = make_node(graph, key, name_generator, parent_nodename)
        
        if entry['__type'] == 'list':
          graph_subtree(entry, graph, name_generator, nodename)


def graph_simple (subtree, graph, name_generator, parent_nodename=None, fontsize=20):
  for key, entry in subtree.items():
    nodename = make_node(graph, key, name_generator, parent_nodename, fontsize=fontsize)
    graph_simple(entry, graph, name_generator, nodename, fontsize=fontsize-2.5)


"""
  Transform the structured font list into a recursive dict, where every entry 
  is a dict in itself.

  {
    str:key: { }
  }
"""
def strip_data_and_fonts (tree):
  return {
    key: strip_data_and_fonts(value) for key, value in tree.items() if key != '__type' and value['__type'] != 'font'
  }


"""
  Merge branches that have only one child into their parent label.
"""
def simplify_sparse_branches (tree):
  tree_simplified = {}

  for key, value in tree.items():
    value_simplified = simplify_sparse_branches(value)

    if len(value_simplified) == 1:
      key_simplified = list(value_simplified.keys())[0]
      tree_simplified[key + ' ' + key_simplified] = value_simplified[key_simplified]
    else:
      tree_simplified[key] = value_simplified
      
  return tree_simplified

fonts = json.load(open('balsa_fonts.json', 'r'))

file_count = 0
variants = []
repository_count = 0

structured_font_list = {}

for project_id in sorted(fonts.keys()):
  repository_count += 1

  project = get_project(int(project_id)).get()
  
  for font in fonts[project_id]:
    file_count += 1

    basename, ext = os.path.splitext(font['name'])
    basename = normalize_basename(basename)
    address_parts = basename.split(' ')

    if basename not in variants:
      variants.append(basename)
    
    insertion_point = create_nested_address_in_tree(address_parts, structured_font_list)
    download_url = DOWNLOAD_URL_PATTERN.format(web_url=project['web_url'], branch=project['default_branch'], path=font['path']).replace(' ', '%20').replace('?', '%3F')
    web_url = FILE_WEB_URL_PATTERN.format(web_url=project['web_url'], branch=project['default_branch'], path=font['path']).replace(' ', '%20').replace('?', '%3F')

    if ext not in insertion_point:
      insertion_point[ext] = {
        '__type': 'font',
        'name': font['name'],
        'basename': basename,
        'web_url': web_url,
        'download_url': download_url,
        'template_safe_name': '{}--{}'.format(basename, ext[1:]),
        'local_path': os.path.join('fonts', font['name']),
        'seasons': [ project ]
      }
    else:
      insertion_point[ext]['seasons'].append(project)

variant_count = len(variants)

print('Repositories: {}'.format(repository_count))
print('Variants: {}'.format(variant_count))
print('File count: {}'.format(file_count))

name_generator = make_name_generator(length=3)
graphname = 'balsa-fonts'
graphname_ume = 'balsa-fonts-ume'

graph = Graph(name=graphname, format='pdf', engine='sfdp')
graph.attr('graph', overlap='false', maxiter='999', repulsiveforce='3')

structure_simplified = simplify_sparse_branches(strip_data_and_fonts(structured_font_list))

del structure_simplified['umetra']

graph_simple(structure_simplified, graph, name_generator, fontsize=20)

graph.render(graphname, format='pdf')
graph.render(graphname, format='svg')

graph_ume = Graph(name=graphname_ume, format='pdf', engine='sfdp')
graph_ume.attr('graph', overlap='false', maxiter='999', repulsiveforce='4')
graph_simple(structure_simplified['ume'], graph_ume, name_generator, make_node(graph_ume, 'ume', name_generator, fontsize=24), fontsize=20)
graph_ume.render(graphname_ume)
