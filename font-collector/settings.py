import os.path 

BASE_DIR = os.path.dirname(__file__)
CACHE_DIR = os.path.join(BASE_DIR, 'cache')
FONT_DIR = os.path.join(BASE_DIR, 'fonts')
API_URL = "https://gitlab.constantvzw.org/api/v4/"
GROUP_ID = 8

DEBUG = False
MAX_PAGES = 999
PER_PAGE = 100

"""
654: work.balsamine.2021-2022
602: work.balsamine.2020-2021
508: work.balsamine.2019-2020
448: work.balsamine.2018-2019
426: archives.balsa
388: foundry.ume
353: work.balsamine.2017-2018
351: work.balsamine.www
177: work.balsamine.2016-2017
176: work.balsamine.2015-2016
175: work.balsamine.2014-2015
174: work.balsamine.2013-2014
173: work.balsamine.2012-2013
172: work.balsamine.2011-2012
121: live.balsa-bootstrap
"""
projects = [ 654, 602, 508, 448, 426, 388, 353, 351, 177, 176, 175, 174, 173, 172, 121 ]

PLACEHOLDER_TEXT = " The association is constituted for an undetermined period. It can be dissolved at any time. "
INDEXED_EXTENSIONS = ['.sfd', '.otf', '.ttf', '.woff', '.woff2', '.ufo']
PREVIEWED_EXTENSIONS = ['.otf', '.ttf', '.woff', '.woff2']

FILE_WEB_URL_PATTERN = '{web_url}/-/blob/{branch}/{path}'
DOWNLOAD_URL_PATTERN = '{web_url}/-/raw/{branch}/{path}'