#! /bin/bash

for FNT in webfonts/*.ttf; do
  cp ${FNT} "${FNT}--unhinted";
  ttfautohint "${FNT}--unhinted" "${FNT}";
  rm "${FNT}--unhinted";
  sfnt2woff "${FNT}";
  woff2_compress "${FNT}";
done;

for FNT in webfonts/*.otf; do
  sfnt2woff "${FNT}";
  woff2_compress "${FNT}";
done;