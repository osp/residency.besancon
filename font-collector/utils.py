import os
import re
import urllib

"""
  Recursive function to create an address, or a path
  in a tree-like data structure.

  Returns a reference to the last created element

  address: list
  tree: dict
"""
def create_nested_address_in_tree (address, tree):
  # If no more address left, return the last leaf
  if len(address) > 0:
    # If current part of the address isn't in the tree yet, create it.
    if address[0] not in tree:
      tree[address[0]] = {
        '__type': 'list'
      }

    return create_nested_address_in_tree(address[1:], tree[address[0]])
  else:
    return tree

"""
  Return a formatted version of the basename.

  Replace '-', '_' for a space.
  Insert a space before capitals.
"""
def normalize_basename (basename):
  basename = basename.replace('-', ' ').replace('_', ' ')
  # Insert a space before every captial that isn't at the
  # start of a sentence
  basename, _ = re.subn(r'((?<!^)(?<!\s)[A-Z])', r' \1', basename)
  return basename.lower()


def download_file (local_path, remote_url):
  local_dir = os.path.dirname(local_path)

  if not os.path.exists(local_dir):
    os.makedirs(local_dir)

  request = urllib.request.urlopen(remote_url)
  with open(local_path, 'b+w') as font:
    font.write(request.read())
