
# UME recipe book

to look at

    pelican --listen

to regenerate

    pelican

recipes

    http://localhost:8000/recipes.html

## one file

for now, all the book is in the `index.html` file.
it contains a loop iterating over all articles and printing each of them.

## categories, order and templates

`content/articles/` is divided in **subfolders**.
each subfolders is a different **category**.
for example the articles under the `recipes/` subfolder will inherit the `recipes` category from pelican.

both categories and articles are **ordered** by `basename` (filename), by default.
so the order of the leaflet is the same that what you see in your file browsing system, and just have to rename them to change the order.

by using the plugins `category_metadata` we can assign pelican **metadata for categories**, outside of only their subfolder name.
those metadatas are found in the `index.md` file inside every of those subfolders, for example `content/articles/recipes/index.md`.
there we can define the `template` to use for every article of this category, and a `title` that doesn't include the ordering number.


## pagedjs

pagedjs has been added to be able to see the pages in the browser mainly.
but maybe we could create them explicitly if we don't need pagedjs to 'page' our document in a more precise way than CSS already support.

## interface CSS/JS

added `print-interface.css` and `interface.js` have been added, taken back from when I (Doriane) did the balsamine 2021-2022 posters. to have some in-browser controls over the layout display.