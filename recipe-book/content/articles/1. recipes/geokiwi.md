---
title: GeoPenDown
tools: Inkscape, python, G-code, plotter
transformations: draw and interpret, get coordinates from SVG paths, convert python dictionary to G-code, send data
process: new
--- 

This recipe leads to a text printed by a plotter whose pen is never pulled up. As the pen never lifts, what you will get is a text composed purely with ligatures. To be honest, this choice was made because there was a breakdown on the Z-axis of our plotter.

## Vector design of the glyphs

You can decide to either start from scratch or to have fun with an existing font.
For the vector design of the glyphs, use the software `Inkscape`. Choose a grid spacing in `File → Document properties` and set snapping to the grid on. For each glyph, you will need to draw a rectangle box and fit the glyph's points into this box. The stroke style for this rectangle box must be set to `#ff0000` (red). Inside of this box, draw your letter with a black single-line path. This path then must be named with the name of the glyph (to do so, go in the `Object → Object properties` tab and set `Label` to the according glyph). 

On this side of the poster, we started from scratch and the grid proportions were: `width: 6`  and `height: 10`. On the reverse side, our starting point was the UmePlumePGothic font. The grid proportions were `width: 12` and `height: 20`. If you want to cook an existing font in this recipe, put it on a background layer, draw the points on another layer on top of it and don't forget to delete the background layer in the end.
When your preparation is ready, save your file as SVG and it's time to put it in the oven.

![Draw the glyphs inside of red rectangles in Inkscape](https://gitlab.constantvzw.org/osp/residency.besancon/-/raw/main/recipe-book/content/img/geopendown.png)

## From SVG to G-CODE

Let's cook your glyphs now. You can do it thanks to the Python library `svgpathtools`. First, download the two scripts `svgpath_encodefont.py` and `svg_to_gcode.py`. Second, make sure Python 3 is installed on your machine. To do so launch the following command in your terminal: 

```
python3 --version
```

It should output the version number of the copy of Python you have installed, something like: `'3.10.7'`.
If the terminal instead prints something like: `'Command 'python3' not found...'`.
It is most likely Python isn't installed. On [python.org](https://python.org/) you can find information on how to install it on your platform. Third, install `svgpathtools` with the command:

```
pip install svgpathtools
```

Then open the script `svgpath_encodefont.py` in your favorite code or text editor. Line 6 of this script looks like this:

```
paths, attributes = svg2paths('x.svg')
```

Replace `x.svg` with the name of the SVG file you just prepared. Your file must be placed in the same folder as the scripts. Once it's done, save and close the file.
Then open the script `svg_to_gcode.py` in your code or text editor. Line 5 of the script is the following:

```
with open('content.txt', 'r') as f:
```
Replace `content.txt` by any other text file you would like to plot. If this is the case, place your text file in the same folder as the scripts and your SVG file. Then on lines 8 and 9, you can set the maximum values in millimeters for the width and height of your plotting surface. If you want to play with stretching the letters, you can change the stretching factors values on X and Y axis on lines 11 and 12. As the coordinates for your glyphs will be values between 0 and 1, it is completely possible to multiply these values to get diffrerent sort of deformations on the basic grid.
Then save the script and it's time to launch it with this command in the terminal:

```
python3 svg_to_gcode.py
```

The script `svg_to_gcode.py` first executes the `svgpath_encodefont.py` script. In this script, `svgpathtools` is used to export each point from the glyphs in your SVG file to a dictionary of X, Y coordinates normalised between 0 and 1. To do so, it will look for the paths in your SVG file, spot those with a red stroke, get the path inside of each box, and associate the path coordinates with the glyph its label contains. When the script has done the job, the `svg_to_gcode.py` script gets the coordinates and transforms it into G-CODE commands, according to the stretching factors you have set and the text to be plotted. Line breaks are made in the hardest way: as soon as you reach the limit width you have set, the script resets the X coordinates to the left edge of the page. Also, because playing with variables is fun, on every line break the stretching factors are randomly reset. So every time you run the script you will get a different output. After the script has ran, you get a `geopendown.gcode` file in the folder you're working in.

![Send the G-CODE to the plotter](https://gitlab.constantvzw.org/osp/residency.besancon/-/raw/main/recipe-book/content/img/geopendown_preview.png)

## Feeding the plotter

You need a CNC controller to connect and control the plotter. You can use either `bCNC` or `Universal G-CODE Sender`. Once you have installed one of these softwares, open it and plug the plotter in one of the USB ports on your laptop. Find the correct port in the software and connect to the machine. Then, unlock the controls and set the origins with the `Jog controller`. When the origins are set, you can run the G-CODE and the plotter starts plotting!

<hr>

<hr>
