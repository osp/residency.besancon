---
title: Helium
caption: UmeHelium.otf
slug: helium
year: 2020-2021
season_balsamine: 10<sup>th</sup> Season. «&#8239;Pas de retour à l'anormal&#8239;»
coverage_pmb: 123
tools: Inkscape, Fontforge
transformations: convert to SVG paths, convert to FontForge curves, bake font
cooking_time: puff pastry
curviness: 5/5
difficulty: 2/5
dirtiness: 1/5
weights: 1
styles: 1
suffix: Mincho
typology: curve
tags: balloon, pathfind
---

Helium is an overinflating process which transforms the points of outlines.

![Transform contour points into auto-smooth]({static}/img/helium-turning-into-curves.png)

The first step is to write, in the same vector file, all the glyphs you wish to process. Here, we used Inkscape.  
Then, vectorise them in `Path → Object to path` and ungroup the shapes in `Object → Ungroup`.

Then, select all the points and force them to turn into curves points. For that, click the button to `make selected nodes auto-smooth`. 

This leads to shapes that will make you a balloon carver.

At this point, it is up to you to adjust the position of the stitches and tengantes to tighten up in certain places, accentuate the plumpness of a shoulder or the flexion of a crossing.

![Carving]({static}/img/helium-full-curves.png)


<hr>