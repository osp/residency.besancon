---
title: Loop
caption: UmeLoopGothic6.otf
slug: loop
year: 2015-2016
season_balsamine: 5<sup>th</sup> season. «&#8239;L’art acte de resistance&#8239;» 
coverage_pmb: 185
cooking_time: granola
curviness: 1/5
difficulty: 3/5
weights: 3
styles: 1
dirtiness: 5/5
tools: laser printer, scanner, GlyphTracer, Fontforge
transformations: prepare document, print document, scan document, transform pixeldata to vectors, bake font
suffix: Gothic
typology: bitmap
tags: crunchy, laser, burn
---

*Loop* is a crunchy recipe to create a –vector– font from bitmap –pixelbased– pictures using GlyphTracer as the central tool. GlyphTracer works as a letter recognition sytem (OCR). Once the tool has recognised the individual letters, or glyphs, you can map each letter to a Unicode code point. In the final step, the tool will convert the individual characters to a vector shape and cast them to FontForge’s data format. The font can then be smoothly baked with FontForge.

## Font to bitmap to font

To start, you need to prepare a proper base picture. 
It can be a picture you created digitally, or a scan of a text or specimen. To create the Ume loop, we printed pictures of the font at different body sizes with both a laser- and inkjet-printer and scanned them in high definition.

You need a 1-bit image (only black and white pixels) of your future glyphs. Make sure there is enough “room” between them. To be more precise, as the letter recognition system is based on white space, every line or row must be separated from other rows by a continuous horizontal strip of white. In the same way, it must have a sufficient space between each glyph –try a bigger letter spacing than the default. If this is not the case –because, for example, your image is tilted, or the ascenders and descenders interlock– detection will fail. 

When importing a bitmap, Fontforge scales the bitmap (up and down) so that it fits into the glyph box.
To prevent ending up with differently scaled glyphs you can add a black rectangle at start or end of the line, the rectangle should cover the distance between the maximum ascender and descender.
<figure><img src="{static}/img/loop-baseline.png"/></figure>

When you feel your composition is ready, generate an image out of it. Then, change its color mode into to indexed with a 1-bit palette, black and white. We used Gimp or ImageMagick, but use any image editing tool you want. The picture format used can be in any format understood by Qt. For our recipe we did use the BMP format, but Qt also supports formats like TIFF and WEBP —don’t hesitate to look at the Qt platform documentation to see the most recent list of supported formats. 

Depending on the quality and definition of the starting image and on the result you want to obtain, it might be necessary to process more finely your starting picture.  

![Searing the freshly created pixels]({static}/img/loop-threshold.png)

Puffing it up and sharpening it will bring more or less softness or crunchiness. 
You can puff up the image simply by scaling it up. This will in fact create extra pixels, so don’t scale up more than 240% in one step to avoid breaking the bitmap patterns –which would result in big chuncks of square pixels. Then, sharpen the picture to strengthen the contrast and sear the freshly created pixels. To do so, you can use an “unsharp mask” –in Gimp– and search the radius point that fits your image and taste. Finish by applying a bit of threshold and save your picture in indexed black and white (1-bit) palette.

## GlyphTracer and Fons

Glyphtracer is the base tool which will do the heavy work of transforming pixels into vector shapes. Through its own experience, OSP did create extra scripts to facilitate the “Post production” design of the font. These scripts are part of the tool we called Fons and its repository also contains GlyphTracer. 
That said, Glyphtracer is not maintained by us, so you should check depedency updates in its README. In this recipe we only focus on the use of GlyphTracer but don’t hesitate to look at Fons' README to try out those extra features <https://gitlab.constantvzw.org/osp/tools.fons>

To start, download the tool repository <https://github.com/jpakkane/glyphtracer>
Then install the dependencies specified in the README file:

```
pip install PyQt5
sudo apt-get install potrace
```

At this point, GlyphTracer is ready to be launched. From the terminal, go to the Glyphtracer folder and run `./glyphtracer`. Once running, feed it the image you created –if it’s not in 1-bit mode, GlyphTrace will complain– and map each shape to a letter. For each given glyph, click on the letter you want to use. You can change the characters subset in the bottom left dropdown menu to select more glyphs.

Once done, you are ready to generate the .sfd file that you can bake with FontForge.

Enjoy your fresh font!

<hr>

<hr>
