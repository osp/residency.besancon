---
title: Plume
caption: UmePlumePGothic-410.otf
slug: plume
year: 2014-2015
season_balsamine: 4th season. "Between score and encyclopedia. A set of chants throuh digital engravings".
coverage_pmb: 280
cooking_time: puff pastry
curviness: 3/5
difficulty: 2/5
weights: 14
styles: 1
dirtiness: 3/5
tools: Inkscape, AutoTrace, python, Fontforge
transformations: convert to SVG paths, rasterize curves to pixels, detect outlines and derive centerline, convert to Fontforge curves, bake font
suffix: Mincho, Gothic
typology: stroke
tags: ductus, roots
---
