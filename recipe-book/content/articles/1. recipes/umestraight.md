---
title: Straight
caption: Ume-Straight-Soft.ttf
slug: straight
year: 2020-2021
season_balsamine: 10th Season. "Pas de retour à l'anormal"
coverage_pmb: 1931
tools: Inkscape, Fontforge
transformations: convert to SVG paths, convert to FontForge curves, bake font
cooking_time: cheese cube apero
curviness: 1/5
difficulty: 1/5
dirtiness: 1/5
weights: 1
styles: 1
suffix: Gothic
typology: curve
tags: Chainsaw
---

## liste des étapes

### étape 1

blbbalbalbal
