---
title: StrokeStroke
caption: ume-stroke-stroke-condensed-int-100-ext-100.ttf
slug: stroke
year: 2017-2018
season_balsamine: 7<sup>th</sup> Season. «&#8239;En toute impudence&#8239;»
coverage_pmb: 230
tools: Inkscape, Stroke Stroke PHP, Python, Fontforge
transformations: draw and interpret, extend SVG code and import, set CSS stroke properties, expand SVG strokes, bake font
cooking_time: puff pastry
curviness: 5/5
difficulty: 5/5
weights: 5
styles: 2
dirtiness: 1/5
suffix: Gothic
typology: stroke
tags: double, cursor
---

This recipe will help you to create a double stroke font. The glyphs of the font are redrawn with an inside and outside stroke on which css parameters –`stroke-width` and `linecap`– can be applied through a web interface. This will give you a wide scope of flavours to generate from the same master.   

## Redrawn by its ductus

In lettering, the ductus is the order, direction, speed and rhythm in which the lines are drawn to compose a letter. On this principle, the first step of this recipe is to redraw all the desired glyphs of a font with Inkscape. Placing the outline of the shape of the glyph in the background, you draw two lines which define the inner and outer skeleton of the shape. Once done, add a class “ext” to the outside stroke and “int” to the inside stroke of each glyph. To do so, you can open the XML editor of Inkscape which gives you a smooth access to the source code of the drawing and the option to edit it.

![Add a class to each stroke](https://gitlab.constantvzw.org/osp/residency.besancon/-/raw/main/recipe-book/content/img/Stroke-stroke-int-sharp.png)  

This first part might feel like a big task but it is worth the effort as it will give you the freedom to not only have a constant line width, but to have two lines that can overlap or diverge to form a kind of up- and downstrokes.  
You can also play with the lines individually and change their linecaps.

When satisfied with your curves, wrap up all the svg files into a folder called `main`. Put aside for the next step. 
<figure style="margin-top: 7px;">
<img src="https://gitlab.constantvzw.org/osp/residency.besancon/-/raw/main/recipe-book/content/img/Stroke-stroke-double-N-sharp.png" /></figure>

## Stroke Stroke interface {: .h-first}

To start, download the tool available at this url: <https://gitlab.constantvzw.org/osp/tools.ume-stroke-stroke>  

Put the original font file –the one you based your svg font on– in the `tmp/` folder of the downloaded tool.

For the tool to work, you’ll need python, PHP and Fontforge (including Fontforge’s python bindings).

Make sure you have all of these dependencies or install them.
```
sudo apt-get install python php fontforge  
python-fontforge
```

Then, from the root level of the repository, run a local server with the following command:
```
php -S localhost:5000
```

The stroke stroke tool is now launched on your `localhost` and you can access it through your browser by going to the url <http://localhost:5000>

From this interface, start by opening the `main/` folder, the one containing your svgs you’ve put aside. You will now see the visual overview of your svg collection and will be ready to spice up your strokes with the css parameters available on the right&nbsp;panel.

![View from the interface](https://gitlab.constantvzw.org/osp/residency.besancon/-/raw/main/recipe-book/content/img/Stroke-stroke-interface-sharp.png)

Once the family is shaped to your taste, you are ready to generate a sfd-file. For this, click on the `form` button, give a name to your font and click on the `save svg` button which will lead you to a link to download your sfd. This generation is made through a Fontforge python script which will replace the glyphs of your original font file by the stroke stroke svgs you just shaped. This process will keep the settings of the original font (like name, kerning and hinting) so you’ll likely want to season them according to their drawings.
