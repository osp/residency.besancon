---
title: Glazing
---

Besançon team: 
    Guillaume Bertrand, 
    Quentin Coussirat, 
    Amélie Dumont, 
    Ludi Loiseau, 
    Sarah Magnan, 
    Adélaïde Racca 
    Doriane Timmermans
    
Brussels team: 
    Einar Andersen,
    Gijs de Heij,
    Pierre Huyghebaert 
    Antoine Gelgon

Tools:
    The graphs are generated with Graphviz and Mermaid.
    The layout was knead in Markdown, moulded with Pelican and backed with CSS and HTML.

Printed in Besançon at the Atelier Superseñor

Paper: 
    Olin regular natural white

Printing technics: 
    Plotters
    Riso, 4 colors
    Laser printer

This residency has been possible with the support of the Drac Bourgogne-Franche-Comté. 
We specially thanks Corinne Gamby, Cie Pernette, 3615, Solène Rahon

Thanks Marie Paule and Patrick for the pleasant apartment with the complete collection of La Hulotte. 




