#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'OSP'
SITENAME = 'UME'
SITEURL = ''

PATH = 'content'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'https://getpelican.com/'),
         ('Python.org', 'https://www.python.org/'),
         ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

# CUSTOM PAREMETERS
# ==============================================================================

# theme path
THEME = 'theme-web'


# ARCHITECTURE
# ==============================================================================

# we use the subfolder inside of articles as subcategories,
# so we can map those categories to template file
# -- one template for recipe
# -- another one for colophon
# -- etc
USE_FOLDER_AS_CATEGORY = True

ARTICLE_PATHS = ['articles/']   # exclude articles outside of this

# everything is in one file (index.html) so we remove the individual pages generation
#ARTICLE_SAVE_AS = ''
PAGE_SAVE_AS = ''
CATEGORY_SAVE_AS = ''
AUTHOR_SAVE_AS = ''
TAG_SAVE_AS = ''

# the "collections" type html pages that we want
DIRECT_TEMPLATES = ['index', 'flowchart']

# categories are by default sorted by alphabetical order
# order article (inside each categories), by alphabetical order of their basename
ARTICLE_ORDER_BY = 'basename'

# DATE
# ==============================================================================

TIMEZONE = 'Europe/Paris'
# by default pelican asks for a date for every article (?)
DEFAULT_DATE = 'fs'


# MD EXTENSION
# ==============================================================================

from markdown_captions import CaptionsExtension
# https://github.com/evidlo/markdown_captions

from md_mermaid import MermaidExtension

MARKDOWN = {
    'extensions': [
        CaptionsExtension(),
        # MermaidExtension()
    ],
    'extension_configs': {
        'markdown.extensions.codehilite': {},
        'markdown.extensions.extra': {},
        'markdown.extensions.meta': {},
        'markdown.extensions.toc': {},
    },
    'output_format': 'html5',
}

# for officially and third party extensions:
# https://python-markdown.github.io/extensions/


# FILTER
# ==============================================================================

def slugify(txt):
    return "_".join(txt.split(" ")).lower().translate({ord(i): None for i in '()'})

def five2glyph(meta, glyph):
    length = int(meta.split('/')[0])
    str = ''.join([ glyph for i in range(length) ])
    if str == '':
        str = 'zero'
    print(meta, length, str)
    return str

JINJA_FILTERS = { 
    'five2glyph': five2glyph,
    'slugify': slugify
}


# PLUGINS
# ==============================================================================

PLUGIN_PATHS = ['plugins']

PLUGINS = [ 'category_meta', 'yaml_metadata']

# --- YAML METADATA
# this is used for event_date allowing us to precise the date of an event as a dictionnary in the metadata
# custom edit line 56: has been edited line 56 so that tags doesn't need to be noted a yaml list, but can be comma list

# --- CATEGORY META
# every category folder as an index with meta-data
# here category are exactly the sections on the /home
# (https://github.com/getpelican/pelican-plugins/tree/master/category_meta)
# custom edit line 84-88: add order metadata support
