# content make template (articulation)

A super minimalist burgeon in the form of **a single generated HTML page using jinja template and markdown + yaml content**.

It can be used for simple html based design like:
* a flyer
* a basic webpage
* experimenting an identity

## explain

* print system with container / layer / pad / bleeds + preview button + text-content class (line height, margin, etc)

## has

* easy upload scripts
* basic templates
* print css toolbox
* yaml & markdown parser
* toogle for every .md page is a page with :target

## todo

* smart typo
* python requierement file
  * typogriphy
  * markdown / yaml parser
* slugify jinja filter
* auto-description and other meta tag (+ twitter cards)
* target_link_blank on / off in mardkown
* yaml parser
* css sheet for print
* different templates: flyer print, .md file is page select

## use

        python3 make.py

The content is in markdown in the `content/` folder.

requierements python modules to generate are: 
* jinja 
* markdown

## upload

to upload to your own website, just transfer the content of the `www/` folder.

or via FTP, you can also create an `.env` file with those variables:

```
FTP_HOST="ftp-domain-name.com"
FTP_LOCATION="/folder/to/copy/"
FTP_USERNAME="username"
FTP_PASSWORD="password"
```

and

        ./ftp-sync.sh


or via SCP (for a server to which you have SSH access), with an `.env` file with those variables:

```
SSH_HOST="domaine-name.com"
SSH_LOCATION="/folder/to/copy/"
SSH_USERNAME="username"
SSH_PASSWORD="password"
```

and

        ./scp-sync.sh

