import jinja2
import os
import sys

# appending a path
sys.path.append('../')

import wikipage
from wikipage.page import get_wikipage, is_header

CONTENT_PATH = 'content/'
TEMPLATE_PATH = 'template.html'
HTML_PATH = 'www/index.html'

options = {
    'print': True,
    'reset': True,
}

# wikipedia_pages = [
#     ['stroke', 'topological skeleton'],
#     ['bitmap', 'image scanner'],
#     ['outline', 'bézier curve'],
# ]
wikipedia_pages = [
    ['stroke', 'knife'],
    ['bitmap', 'spoon'],
    ['outline', 'fork'],
]

if __name__ == '__main__':

    # --- WIKI REQUEST ---
    pages = []
    print('--- WIKI ---')
    for request in wikipedia_pages:

        # request wikipage (either from cache or internet)
        text = get_wikipage(request[1])

        if text == '':
            sys.exit("page empty")

        # remove headers
        lines = []
        # lines = text.split('\n')[:6]
        for line in text.split('\n'):
            if not is_header(line) and line.strip() != '':
                lines.append(line)

        # make a page object
        page = {
            'id': request[0],
            'title': request[1],
            'lines': lines,
        }

        pages.append(page)


    # jinja environment
    templateLoader = jinja2.FileSystemLoader( searchpath="" )
    env = jinja2.Environment(
        loader=templateLoader
    )

    # getting the template
    template = env.get_template(TEMPLATE_PATH)

    # render template
    html = template.render(pages = pages, options = options)
    with open(HTML_PATH, 'w') as file:
        file.write(html)