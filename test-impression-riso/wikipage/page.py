from genericpath import isfile
import wikipedia
import os

cache_folder = 'wikicache/'

# wikipedia.set_lang("fr")

def is_header(line):
    # return a couple of header_text and header_level
    # or return nothing if line is not a header

    line = line.strip()

    if line == '':
        return

    i = 0
    while line[i] == '=' and line[-1-i] == '=':
        i += 1

    if i == 0:
        return

    header_text = line[i:(-1-i)].strip()
    header_level = i
    return [header_text, header_level]

def get_wikipage(pagename):
    # get wikipedia page content by name of the page

    path = cache_folder + pagename + '.txt'
    print(path)

    # is in cache
    if os.path.isfile(path):
        print('is in cache')
        with open(path, 'r') as file:
            return file.read()

    # request file
    else:
        print('requesting')
        try:
            results = wikipedia.search(pagename, results=1, suggestion=False)
            try:
                pagename = results[0]
            except IndexError:
                # if there is no suggestion or search results, the page doesn't exist
                raise wikipedia.PageError(pagename)
            page = wikipedia.WikipediaPage(pagename, redirect=True, preload=True)
        except wikipedia.exceptions.DisambiguationError as e:
            print(e.options)
            page = ''

        # save in cache
        print(page)
        with open(path, 'w') as file:
            file.write(page.content)

        return page.content
        

