
// init elements
let class_checkboxes = document.getElementsByClassName('class-checkbox');


// BODY CLASS CHECKBOXES
// --------------------------------------------------------
function toggle_class(classname, val){
  if(val){
    document.body.classList.add(classname);
  }
  else{
    document.body.classList.remove(classname);
  }
}

for(let checkbox of class_checkboxes){
  let classname = checkbox.dataset.class;
  checkbox.addEventListener('input', function(){
    toggle_class(classname, checkbox.checked);
  });
  toggle_class(classname, checkbox.checked);
}
